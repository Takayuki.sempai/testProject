package epam.lesson1;

import epam.lesson1.task1.ObjectName;
import epam.lesson1.task2.Line;
import epam.lesson1.task2.Point;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Prokofiev-VA
 * Class of utilities for I/O organizing
 */
public final class IOUtils {
    private IOUtils() {}

    /**
     * Formatted print collection
     * @param format format for printing one element
     * @param elements collection for printing
     */
    public static void printCollection(String format, Collection<?> elements) {
        for (Object o: elements) {
            System.out.printf(format, o);
        }
    }

    /**
     * formatted print map
     * @param format format for printing one element
     * @param elements map for printing
     */
    public static void printMap(String format, Map<?, ?> elements) {
        for (Map.Entry<?, ?> pair: elements.entrySet()) {
            System.out.printf(format, pair.getKey(), pair.getValue());
        }
    }

    /**
     * Getting list of points from user input
     * @return list of points
     */
    public static Set<Point> getPoints() {
        Set<Point> points = new HashSet<>();
        System.out.println("Enter the points in format x y. Complete the input with any non-numeric character");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLong()) {
            long x = scanner.nextInt();
            if(!scanner.hasNextLong()) {
                System.out.println("Pair non complete. The first coordinate will by ignored");
                break;
            }
            long y = scanner.nextInt();
            points.add(new Point(x, y));
        }
        return points;
    }

    /**
     * Printing map to file
     * @param format format for printing one element
     * @param elements map for printing
     */
    public static void map2File(String format, String fileName, Map<Line, Set<Point>> elements) {
        try(FileWriter fw = new FileWriter(fileName)) {
            for (Map.Entry<Line, Set<Point>> pair: elements.entrySet()) {
                fw.write(String.format(format, pair.getKey(), pair.getValue().size()));
            }
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getting file name from user input
     * @return file name
     */
    public static String getFileName() {
        System.out.println("Enter file name:");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    /**
     * Reading list object "ObjectName" from file
     * @param fileName file name
     * @return objects list
     */
    public static List<ObjectName> readFromFile(String fileName) {
        List<ObjectName> elements = new ArrayList<>();
        try (BufferedReader fr = new BufferedReader(new FileReader(fileName))){
            String line = fr.readLine();
            while (line != null) {
                String[] keyValue = line.split("=", 2);
                elements.add(new ObjectName(keyValue[0], keyValue[1]));

                line = fr.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elements;
    }
}
