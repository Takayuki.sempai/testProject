package epam.lesson1.task1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by Prokofiev-VA
 * Class of utilities for working with ObjectName lists
 */
public final class ListUtils {

    private ListUtils() {}

    /**
     * Removing duplicate items from the list
     * @param list objects list
     * @return compressed list
     */
    public static List<ObjectName> listCompressing(List<ObjectName> list) {
        return new ArrayList<>(new LinkedHashSet<>(list));
    }

    /**
     * Sorting list
     * @param list objects list
     */
    public static void listSorting(List<ObjectName> list) {
        list.sort(new IdComparator());
    }
}
