package epam.lesson1.task1;

import java.util.Comparator;

/**
 * Created by Prokofiev-VA
 * Class for comparing objects by id
 */
public class IdComparator implements Comparator<ObjectName> {
    @Override
    public int compare(ObjectName o1, ObjectName o2) {
        return o1.getId().compareTo(o2.getId());
    }
}
