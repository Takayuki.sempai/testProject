package epam.lesson1.task1;

import epam.lesson1.IOUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Prokofiev-VA
 * Class for execution task 1:
 * The input file stores the names of some objects.
 * Construct a list of C1, whose elements contain the names and ciphers of these objects,
 * with the list items being ordered in ascending order of ciphers.
 * Then "compress" the C1 list, deleting duplicate object names.
 */
public class Task1 {
    public static void main(String[] args) {
        Task1 app = new Task1();
        app.start();
    }

    private void start() {
        String fileName = IOUtils.getFileName();
        List<ObjectName> C1 = IOUtils.readFromFile(fileName);

        Task1Logger.logScannedData(C1);

        ListUtils.listSorting(C1);
        Task1Logger.logSortedData(C1);

        C1 = ListUtils.listCompressing(C1);
        Task1Logger.logCompressedData(C1);
    }
}
