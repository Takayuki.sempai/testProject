package epam.lesson1.task1;

/**
 * Created by Prokofiev-VA
 * Class to storing properties of objects
 */
public class ObjectName {
    private static final String FORMAT = "name='%s', id='%s'";
    private String name;
    private String id;

    /**
     * Creating new object
     * @param name object name
     * @param id object is
     */
    public ObjectName(String name, String id) {
        this.name = name;
        this.id = id;
    }

    /**
     * Getting object name
     * @return object name
     */
    public String getName() {
        return name;
    }

    /**
     * Getting object id
     * @return object id
     */
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.format(FORMAT, name, id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ObjectName that = (ObjectName) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
