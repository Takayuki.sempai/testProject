package epam.lesson1.task1;

import epam.lesson1.IOUtils;

import java.util.List;

/**
 * Created by Prokofiev-VA
 * Class for logging work results
 */
public final class Task1Logger {
    private Task1Logger() {}

    /**
     * Printing read data
     * @param elements list of ObjectName
     */
    public static void logScannedData(List<ObjectName> elements) {
        System.out.println("Read data:");
        IOUtils.printCollection("%s%n", elements);
    }

    /**
     * Printing sorted data
     * @param elements list of ObjectName
     */
    public static void logSortedData(List<ObjectName> elements) {
        System.out.println("Sorted data:");
        IOUtils.printCollection("%s%n", elements);
    }

    /**
     * Printing compressed data
     * @param elements list of ObjectName
     */
    public static void logCompressedData(List<ObjectName> elements) {
        System.out.println("Compressed data:");
        IOUtils.printCollection("%s%n", elements);
    }
}
