package epam.lesson1.task2;

import epam.lesson1.IOUtils;

import java.util.Map;
import java.util.Set;

/**
 * Created by Prokofiev-VA
 * Class for logging work results
 */
public final class Task2Logger {

    private Task2Logger() {}

    /**
     * Printing set of point to console
     * @param points list of points
     */
    public static void printPoints(Set<Point> points) {
        System.out.println("Entered points:");
        IOUtils.printCollection("Point: %s%n", points);
        System.out.println();
    }

    /**
     * Printing results of work
     * @param elements map: line - number of points to it
     */
    public static void printMap(Map<Line, Set<Point>> elements) {
        System.out.println("Results:");
        IOUtils.printMap("The line %s contains %s %n", elements);
        System.out.println();
    }
}
