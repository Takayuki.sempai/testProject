package epam.lesson1.task2;

import java.util.*;

/**
 * Created by Prokofiev-VA
 * Class of utilities for working with lines
 */
public final class LineUtils {
    private LineUtils() {}

    /**
     * Creating all possible lines passing throw at least 2 points
     * @param points list of points for the created lines
     * @return set of lines
     */
    public static Map<Line, Set<Point>> createLines(Set<Point> points) {
        Map<Line, Set<Point>> lines = new HashMap<>();
        List<Point> pointsList = new ArrayList<>(points);
        for (int i = 0; i < points.size() - 1; i++) {
            for (int j = i+1; j < points.size(); j++) {
                Line line = new Line(pointsList.get(i), pointsList.get(j));
                if(lines.containsKey(line)) {
                    lines.get(line).add(pointsList.get(j));
                } else {
                    Set<Point> pointsSet = new HashSet<>();
                    pointsSet.add(pointsList.get(i));
                    pointsSet.add(pointsList.get(j));
                    lines.put(line, pointsSet);
                }
            }
        }
        return lines;
    }
}
