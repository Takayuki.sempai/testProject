package epam.lesson1.task2;

/**
 * Created by Prokofiev-VA
 * Class for line of type ax+by+c=0
 */
public class Line {
    private static final String FORMAT = "%dx%+dy%+d=0";
    private long a;
    private long b;
    private long c;

    /**
     * Creating a line by two points
     * @param point1 first point
     * @param point2 second point
     */
    public Line(Point point1, Point point2){
        a = point1.getY() - point2.getY();
        b = point2.getX() - point1.getX();
        c = point1.getX() * point2.getY() - point2.getX() * point1.getY();
        normalize();
    }

    /**
     * Division of line coefficients by the greatest common divisor
     */
    private void normalize() {
        long divider = gcd(gcd(Math.abs(a), Math.abs(b)), Math.abs(c));
        if(divider != 0) {
            a /= divider;
            b /= divider;
            c /= divider;
        }
    }

    /**
     * Calculation of the greatest common divisor
     * @param firstNumber first number
     * @param secondNumber second number
     * @return greatest common divisor
     */
    private long gcd(long firstNumber, long secondNumber) {
        if(secondNumber == 0) return firstNumber;
        return gcd(secondNumber, firstNumber % secondNumber);
    }

    /**
     * Check if the point of the line belongs
     * @param p point for check
     * @return true if the point belongs the line
     */
    public boolean isBelongsLine(Point p) {
        return a * p.getX() + b * p.getY() + c == 0;
    }

    @Override
    public String toString() {
        return String.format(FORMAT, a, b, c);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Line line = (Line) o;

        if (a != line.a) return false;
        if (b != line.b) return false;
        return c == line.c;
    }

    @Override
    public int hashCode() {
        int result = (int) (a ^ (a >>> 32));
        result = 31 * result + (int) (b ^ (b >>> 32));
        result = 31 * result + (int) (c ^ (c >>> 32));
        return result;
    }
}
