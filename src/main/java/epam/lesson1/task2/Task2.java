package epam.lesson1.task2;

import epam.lesson1.IOUtils;

import java.util.*;

/**
 * Created by Prokofiev-VA
 * Class for execution task 2:
 * There are N points on the plane.
 * Output to the file descriptions of all the lines that pass more than one point from the given.
 * For each line, specify how many points it passes. Use the HashMap class.
 */
public class Task2 {
    public static void main(String[] args) {
        Task2 app = new Task2();
        app.start();
    }

    private void start() {
        Set<Point> points = IOUtils.getPoints();
        Task2Logger.printPoints(points);

        Map<Line, Set<Point>> lines = LineUtils.createLines(points);
        Task2Logger.printMap(lines);

        String fileName = IOUtils.getFileName();
        IOUtils.map2File("The line %s belongs %d points%n", fileName, lines);
    }
}
