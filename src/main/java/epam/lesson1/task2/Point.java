package epam.lesson1.task2;

/**
 * Created by Prokofiev-VA
 * Class for two-dimensional point
 */
public class Point {
    private static final String FORMAT = "(x=%s, y=%s)";
    private long x;
    private long y;

    /**
     * Creating two-dimensional point
     * @param x first coordinate
     * @param y second coordinate
     */
    public Point(long x, long y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Getting the coordinate x
     * @return x coordinate
     */
    public long getX() {
        return x;
    }

    /**
     * Getting the coordinate y
     * @return y coordinate
     */
    public long getY() {
        return y;
    }

    @Override
    public String toString() {
        return String.format(FORMAT, x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        return y == point.y;
    }

    @Override
    public int hashCode() {
        int result = (int) (x ^ (x >>> 32));
        result = 31 * result + (int) (y ^ (y >>> 32));
        return result;
    }
}
