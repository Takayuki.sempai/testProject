package epam.lesson1.test;

import epam.lesson1.IOUtils;
import epam.lesson1.task2.Line;
import epam.lesson1.task2.LineUtils;
import epam.lesson1.task2.Point;
import epam.lesson1.task2.Task2;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Prokofiev-VA
 * Class for testing task2
 */
public class Task2Test {

    @Test
    public void linesTest() {
        System.out.println("Test: linesCountTest");
        Set<Point> points = new HashSet<>(Arrays.asList(
                new Point(1, 3),
                new Point(5, 28),
                new Point(3, 14),
                new Point(89, 4),
                new Point(7, 18),
                new Point(32, 1),
                new Point(14, 7)));

        System.out.println("Points:");
        IOUtils.printCollection("Point: %s%n", points);

        Map<Line, Set<Point>> lines = LineUtils.createLines(points);

        System.out.println("Lines:");
        IOUtils.printCollection("Line: %s%n", lines.keySet());

        Assert.assertEquals(21, lines.size());
    }

    @Test
    public void sameLinesTest() {
        System.out.println("Test: sameLinesTest");
        Set<Point> points = new HashSet<>(Arrays.asList(
                new Point(1, 1),
                new Point(2, 2),
                new Point(3, 3),
                new Point(4, 4),
                new Point(5, 5),
                new Point(32, 1),
                new Point(14, 7)));

        System.out.println("Points:");
        IOUtils.printCollection("Point: %s%n", points);

        Map<Line, Set<Point>> lines = LineUtils.createLines(points);

        System.out.println("Lines:");
        IOUtils.printCollection("Line: %s%n", lines.keySet());

        Assert.assertEquals(12, lines.size());
    }

    @Test
    public void sameLinesCountTest() {
        System.out.println("Test: sameLinesCountTest");
        Set<Point> points = new HashSet<>(Arrays.asList(
                new Point(1, 1),
                new Point(2, 2),
                new Point(3, 3),
                new Point(4, 4),
                new Point(5, 5),
                new Point(32, 1),
                new Point(14, 7)));

        System.out.println("Points:");
        IOUtils.printCollection("Point: %s%n", points);

        Map<Line, Set<Point>> lines = LineUtils.createLines(points);

        System.out.println("Lines:");
        IOUtils.printMap("The line %s contains %s %n", lines);

        for (Set<Point> pointsForLine: lines.values()) {
            if(pointsForLine.size() == 5) {
                return;
            }
        }
        Assert.fail("The line passing through 5 points was nor found");
    }

    @Test
    public void writeFileTest() {
        System.out.println("Тест: writeFileTest");
        Set<Point> points = new HashSet<>(Arrays.asList(
                new Point(1, 1),
                new Point(2, 2),
                new Point(3, 3),
                new Point(4, 4),
                new Point(5, 5),
                new Point(32, 1),
                new Point(14, 7)));

        Map<Line, Set<Point>> lines = LineUtils.createLines(points);

        String fileName = "test.txt";
        System.out.println("Writing to file " + fileName);
        IOUtils.map2File("The line %s belongs %d points%n", fileName, lines);
        Assert.assertEquals("The file is not recorded correctly", 12, checkFile(fileName));
    }

    private int checkFile(String fileName) {
        int linesCount = 0;
        try (BufferedReader fr = new BufferedReader(new FileReader(fileName))){
            while (fr.readLine() != null) {
                linesCount++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Assert.fail("Error riding file");
        }
        return linesCount;
    }
}
