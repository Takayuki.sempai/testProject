package epam.lesson1.test;

import epam.lesson1.IOUtils;
import epam.lesson1.task1.ListUtils;
import epam.lesson1.task1.ObjectName;
import epam.lesson1.task1.Task1;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Prokofiev-VA
 * Class for testing task2
 */
public class Task1Test {

    @Test
    public void readFileTest() {
        System.out.println("Test: writeFileTest");

        String fileName = "testTask1.txt";
        List<ObjectName> elements = IOUtils.readFromFile(fileName);
        System.out.println("Read data:");
        IOUtils.printCollection("%s%n", elements);
        Assert.assertEquals("The file is not valid", 7, elements.size());
    }

    @Test
    public void sortTest() {
        System.out.println("Test: sortTest");
        String fileName = "testTask1.txt";
        List<ObjectName> elements = IOUtils.readFromFile(fileName);
        ListUtils.listSorting(elements);
        System.out.println("Sorted data:");
        IOUtils.printCollection("%s%n", elements);
        Assert.assertEquals("Sorting is incorrect", "42", elements.get(0).getId());
    }

    @Test
    public void compressTest() {
        System.out.println("Test: compressTest");
        String fileName = "testTask1.txt";
        List<ObjectName> elements = IOUtils.readFromFile(fileName);
        elements = ListUtils.listCompressing(elements);
        System.out.println("Compressed data:");
        IOUtils.printCollection("%s%n", elements);
        Assert.assertEquals("Compressing is incorrect", 6, elements.size());
    }
}
